// Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.
function map(fn, array) {
   let newArr = [];
   for (let i=0; i<array.length; i++){
      newArr.push(fn(array[i]));
   }
   return newArr;
}
function fn (x) {
   return x**2;
}
document.write(map(fn, [1, 2, 3])+"<hr/>"); // 1,4,9
document.write(map(fn, [1, 1, 3, 5, 8, 2])+"<hr/>"); // 1,1,9,25,64,4

// Використовуючи CallBack function, створіть калькулятор, який буде від користувача приймати 2 числа і знак арифметичної операції. При введенні не числа або при розподілі на 0 виводити помилку.
const rez = (a, b, znak) => {
   if (znak == "+") {
      return a + b;
   }
   if (znak == "-") {
      return a - b;
   }
   if (znak == "*") {
      return a * b;
   }
   if (znak == "/" && b !=0) {
      return a / b;
   } else {
      return "error";
   }
}

function calculait (callback) {
   const a = Number(prompt('Введіть число a'));
   if (isNaN(a)) return document.write("error");   
   const b = Number(prompt('Введіть число b'));
   if (isNaN(b)) return document.write("error");
   const znak = prompt('Введіть знак арифметичної операції');
   document.write(`${a} ${znak} ${b} = `, callback(a, b, znak));
}

calculait (rez);